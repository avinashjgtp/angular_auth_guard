import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { RouterModule } from '@angular/router'
import { dashboardRoutes } from './dashboard.routes';
import { AuthGuardService} from '../guards/auth-guard.service'
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forChild(dashboardRoutes)
  ],
  providers:[AuthGuardService]
  ,
  declarations: [LayoutComponent, HomeComponent, AdminComponent]
})
export class DashboardModule { }
